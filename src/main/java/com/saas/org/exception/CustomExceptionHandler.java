package com.saas.org.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler{

	HttpStatus httpStatus;
	@ExceptionHandler(UserDefinedException.class)
	  public final ResponseEntity<Object> handleUserNotFoundException(UserDefinedException ex, WebRequest request) {
		  List<String> details = new ArrayList<>();
		  details.add("Org Already exists");
		  ErrorResponse exceptionResponse = new ErrorResponse(new Date(),request.getDescription(false),details);
	    return new ResponseEntity<Object>(exceptionResponse, httpStatus.BAD_REQUEST);
	  }

}
