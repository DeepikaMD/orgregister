package com.saas.org.exception;


public class UserDefinedException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserDefinedException(String details) {
		 super(details);
		}
	
}
