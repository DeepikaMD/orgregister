package com.saas.org.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.saas.org.entity.OrgRegister;
import com.saas.org.service.OrgRegisterApplicationService;



@RestController
@RequestMapping(value="/org")
public class OrgRegisterApplicationController {

	@Autowired
	OrgRegisterApplicationService orgService;
	
	@PostMapping(value="/register")
	public ResponseEntity<Object> registerOrg(@RequestBody OrgRegister orgRegister){
		String response = orgService.registerOrg(orgRegister);
		if(response !=null) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
	}
}
