package com.saas.org.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@SequenceGenerator(name="seq", initialValue=1000, allocationSize=1)
@Table(name = "ORGANISATION")
@ApiModel(description = "All details about the Employee. ")
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrgRegister {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
	@ApiModelProperty(notes = "Database generated organisation ID")
	private int orgId;
	
	@ApiModelProperty(notes = "The name of the organisation")
	@Column(name = "org_name")
	private String orgName;
	
	@ApiModelProperty(notes = "The name of the organisation")
	@Column(name = "org_addr")
	private String orgAddr;

	@ApiModelProperty(notes = "The name of the organisation")
	@Column(name = "org_emailId")
	private String orgEmailId;

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgAddr() {
		return orgAddr;
	}

	public void setOrgAddr(String orgAddr) {
		this.orgAddr = orgAddr;
	}

	public String getOrgEmailId() {
		return orgEmailId;
	}

	public void setOrgEmailId(String orgEmailId) {
		this.orgEmailId = orgEmailId;
	}
	
	
	
}
