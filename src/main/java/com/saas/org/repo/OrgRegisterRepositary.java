package com.saas.org.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.saas.org.entity.OrgRegister;

@Repository
public interface OrgRegisterRepositary extends CrudRepository<OrgRegister,String> {

	@Query(value= "SELECT * FROM ORGANISATION WHERE ORG_NAME=?1", nativeQuery = true)
	public OrgRegister findbyOrgName(@Param("orgName")String orgName);
	
}
