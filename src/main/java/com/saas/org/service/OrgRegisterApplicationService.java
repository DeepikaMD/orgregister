package com.saas.org.service;


import org.springframework.stereotype.Component;

import com.saas.org.entity.OrgRegister;

@Component
public interface OrgRegisterApplicationService {
	
public String registerOrg(OrgRegister org);
	
}
