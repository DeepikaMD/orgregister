package com.saas.org.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.saas.org.entity.OrgRegister;
import com.saas.org.exception.UserDefinedException;
import com.saas.org.repo.OrgRegisterRepositary;
import com.saas.org.service.OrgRegisterApplicationService;

@Service
public class OrgRegisterApplicationServiceImpl implements OrgRegisterApplicationService{

	@Autowired
	OrgRegisterRepositary orgRepo;
	
	public String registerOrg(OrgRegister org) {
		JsonObject jObj = new JsonObject();
		String orgName = org.getOrgName();
		
		OrgRegister response = orgRepo.findbyOrgName(orgName);
		if(response==null) {
			orgRepo.save(org);
		}else{
			if(org.getOrgName().equalsIgnoreCase(response.getOrgName())) {
				throw new UserDefinedException("Org already exist!");
			}
		}
		
		String status = jObj.toString();
		return status;
	}
}
