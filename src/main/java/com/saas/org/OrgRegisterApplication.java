package com.saas.org;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrgRegisterApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrgRegisterApplication.class, args);
	}

}
